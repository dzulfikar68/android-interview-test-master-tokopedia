package com.tokopedia.oilreservoir

/**
 * Created by fwidjaja on 2019-09-24.
 */
object Solution {
    fun collectOil(height: IntArray): Int {
        // TODO, return the amount of oil blocks that could be collected
        // below is stub

        var ans = 0
        var i = 0
        var j: Int = height.size - 1
        var res = 0
        while (i < j) {
            if (height.get(i) <= height.get(j)) {
                res = height.get(i) * (j - i)
                i++
            } else {
                res = height.get(j) * (j - i)
                j--
            }
            if (res > ans) ans = res
        }
        return ans
    }
}
