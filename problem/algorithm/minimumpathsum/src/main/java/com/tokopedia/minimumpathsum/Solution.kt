package com.tokopedia.minimumpathsum

object Solution {
    private fun getMin(val1: Int, val2: Int): Int {
        return if (val1 < val2) val1 else val2
    }
    fun minimumPathSum(matrix: Array<IntArray>): Int {
        // TODO, find a path from top left to bottom right which minimizes the sum of all numbers along its path, and return the sum
        // below is stub

        for (i in matrix.indices) {
            for (j in matrix.get(i).indices) {
                if (i == 0 && j == 0) {
                    continue
                } else if (i == 0) {
                    matrix.get(i)[j] = matrix.get(i).get(j) + matrix.get(i).get(j - 1)
                } else if (j == 0) {
                    matrix.get(i)[j] = matrix.get(i).get(j) + matrix.get(i - 1).get(j)
                } else {
                    matrix.get(i)[j] = getMin(matrix.get(i - 1).get(j) + matrix.get(i).get(j), matrix.get(i).get(j - 1) + matrix.get(i).get(j))
                }
            }
        }
        val r: Int = matrix.size - 1
        val c: Int = matrix.get(0).size - 1
        return matrix.get(r).get(c)
    }

}
