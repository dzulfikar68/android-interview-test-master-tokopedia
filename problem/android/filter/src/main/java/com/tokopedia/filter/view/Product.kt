package com.tokopedia.filter.view

data class Product(
    val id: Int?,
    val title: String?,
    val price: Long?,
    val location: String?,
    val image: String?
)