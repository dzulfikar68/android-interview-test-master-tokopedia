package com.tokopedia.filter.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tokopedia.filter.R

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    var listProduct = ArrayList<Product>()

    fun setList(products: List<Product>) {
        this.listProduct.clear()
        this.listProduct.addAll(products)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val weather = listProduct[position]
        holder.bind(weather)
    }

    override fun getItemCount(): Int = listProduct.size

    class ProductViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind(product: Product) {
            with(view) {
                findViewById<TextView>(R.id.title).text = product.title
                findViewById<TextView>(R.id.price).text = "Rp " + product.price
                findViewById<TextView>(R.id.location).text = product.location
                Glide.with(this).load(product.image).into(findViewById(R.id.image))
            }
        }
    }
}