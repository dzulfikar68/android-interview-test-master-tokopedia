package com.tokopedia.filter.view

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.slider.Slider
import com.tokopedia.filter.R
import kotlin.math.roundToLong

class ProductFilterDialogFragment(private var callback: FilterCallback?, private var listProduct: List<Product>?): BottomSheetDialogFragment() {
    companion object {
        fun newInstance(callback: FilterCallback?, listProduct: List<Product>?):
                ProductFilterDialogFragment = ProductFilterDialogFragment(callback, listProduct)
    }

    private lateinit var fragmentView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_dialog_filter, container)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity != null) {
            val locationAdapter = LocationAdapter()
            with(fragmentView.findViewById<RecyclerView>(R.id.rv_location)) {
                layoutManager = StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL)
                setHasFixedSize(true)
                adapter = locationAdapter
            }
            val mostLocations = getMostLocations()
            locationAdapter.setList(mostLocations)
            setSlideMinMax()
            fragmentView.findViewById<Slider>(R.id.sdr_price).setOnChangeListener { slider, value ->
                fragmentView.findViewById<TextView>(R.id.tv_price).text = "Rp " + value.roundToLong().toString()
            }
            fragmentView.findViewById<Button>(R.id.btn_submit).setOnClickListener {
                val loc = if (fragmentView.findViewById<CheckBox>(R.id.cb_location).isChecked) {
                    locationAdapter.selectedLocation
                } else null
                val prc = if (fragmentView.findViewById<CheckBox>(R.id.cb_price).isChecked) {
                    fragmentView.findViewById<Slider>(R.id.sdr_price).value.toString()
                } else null
                callback?.onClick(loc, prc, mostLocations)
                dismiss()
            }
        }
    }

    private fun setSlideMinMax() {
        val slide = fragmentView.findViewById<Slider>(R.id.sdr_price)
        val startPrice = (listProduct?.minBy { it.price ?: 0 }?.price ?: 0L).toFloat()
        val endPrice = (listProduct?.maxBy { it.price ?: 0 }?.price ?: 0L).toFloat()
        slide.valueFrom = 0.0f
        slide.valueTo = endPrice
        slide.value = startPrice
    }

    private fun getMostLocations(): List<String> {
        val mostLocations = listProduct?.groupingBy{ it.location }?.eachCount()?.toList()?.sortedByDescending { it.second }?.take(2)
        val firstLocation = mostLocations?.get(0)?.first
        val secondLocation = mostLocations?.get(1)?.first
        val otherLocation = "Other"
        return listOfNotNull(firstLocation, secondLocation, otherLocation)
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    interface FilterCallback {
        //price or location or both
        fun onClick(location: String?, price: String?, priceRank: List<String>)
    }
}