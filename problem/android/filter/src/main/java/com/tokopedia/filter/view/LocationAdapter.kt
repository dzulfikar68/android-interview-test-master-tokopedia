package com.tokopedia.filter.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.tokopedia.filter.R

class LocationAdapter : RecyclerView.Adapter<LocationAdapter.LocationViewHolder>() {
    var listLocation = ArrayList<String>()
    var selectedLocation: String? = null

    fun setList(locations: List<String>) {
        this.listLocation.clear()
        this.listLocation.addAll(locations)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.location_item, parent, false)
        return LocationViewHolder(view)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val location = listLocation[position]
        holder.bind(location, selectedLocation) {
            selectedLocation = it
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int = listLocation.size

    class LocationViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind(location: String, selectedLocation: String?, onSelected: (String) -> Unit) {
            with(view.findViewById<Chip>(R.id.title)) {
                text = location
                isChecked = location == selectedLocation
                setOnClickListener {
                    onSelected.invoke(location)
                }
            }
        }
    }
}