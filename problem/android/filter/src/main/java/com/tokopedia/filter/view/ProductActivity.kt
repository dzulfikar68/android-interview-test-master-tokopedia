package com.tokopedia.filter.view

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.tokopedia.filter.R
import org.json.JSONException
import org.json.JSONObject
import java.io.*


class ProductActivity : AppCompatActivity() {

    private lateinit var productAdapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        productAdapter = ProductAdapter()
        with(findViewById<RecyclerView>(R.id.product_list)) {
            layoutManager = GridLayoutManager(context, 2)
            setHasFixedSize(true)
            adapter = productAdapter
        }
        val myProducts = getProducts()
        productAdapter.setList(myProducts)
        if (myProducts.isNullOrEmpty()) findViewById<TextView>(R.id.tv_empty).visibility = View.VISIBLE else findViewById<TextView>(R.id.tv_empty).visibility = View.GONE

        findViewById<FloatingActionButton>(R.id.fab_filter).setOnClickListener {
            val dialog = ProductFilterDialogFragment.newInstance(object : ProductFilterDialogFragment.FilterCallback {
                override fun onClick(location: String?, price: String?, priceRank: List<String>) {
                    val firstData = if (location != null) {
                        if (location == "Other") getProducts().filter { !priceRank.contains(it.location) }
                        else getProducts().filter { it.location == location }
                    } else getProducts()
                    val secondData = if (price != null) firstData.filter { it.price == price.toFloat().toLong() } else firstData
                    productAdapter.setList(secondData)
                }
            }, myProducts)
            dialog.show(supportFragmentManager, ProductFilterDialogFragment::class.java.simpleName)
        }
    }

    private fun getProducts(): List<Product> {
        val list = ArrayList<Product>()
        try {
            val responseObject = JSONObject(getRawProductJSON())
            val data = responseObject.getJSONObject("data")
            val products = data.getJSONArray("products")
            for (i in 0 until products.length()) {
                val item = products.getJSONObject(i)

                val id = item.getInt("id")
                val title = item.getString("name")
                val price = item.getLong("priceInt")
                val location = item.getJSONObject("shop").getString("city")
                val image = item.getString("imageUrl")

                val result = Product(id, title, price, location, image)
                list.add(result)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return list
    }

    private fun getRawProductJSON(): String {
        val `is`: InputStream = resources.openRawResource(R.raw.products)
        val writer: Writer = StringWriter()
        val buffer = CharArray(1024)
        var n: Int

        try {
            val reader: Reader = BufferedReader(InputStreamReader(`is`, "UTF-8"))
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        } finally {
            `is`.close()
        }

        val jsonString: String = writer.toString()
        return jsonString
    }
}