package com.tokopedia.maps

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mapFragment: SupportMapFragment? = null
    private var googleMap: GoogleMap? = null

    private lateinit var textCountryName: TextView
    private lateinit var textCountryCapital: TextView
    private lateinit var textCountryPopulation: TextView
    private lateinit var textCountryCallCode: TextView

    private var editText: EditText? = null
    private var buttonSubmit: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        bindViews()
        initListeners()
        loadMap()
    }

    private fun bindViews() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        editText = findViewById(R.id.editText)
        buttonSubmit = findViewById(R.id.buttonSubmit)
        textCountryName = findViewById(R.id.txtCountryName)
        textCountryCapital = findViewById(R.id.txtCountryCapital)
        textCountryPopulation = findViewById(R.id.txtCountryPopulation)
        textCountryCallCode = findViewById(R.id.txtCountryCallCode)
    }

    private fun initListeners() {
        buttonSubmit!!.setOnClickListener {
            // TODO
            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Now Loading")
            progressDialog.show()
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://restcountries-v1.p.rapidapi.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            val service = retrofit.create(RapidApiService::class.java)
            service.getCountryByName(country = editText?.text?.toString()?.trim() ?: "").enqueue(object : Callback<List<Country>> {
                override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                    val data = response.body()?.get(0)
                    val lat = data?.latlng?.get(0) ?: 0.0
                    val long = data?.latlng?.get(1) ?: 0.0
                    val ll = LatLng(lat, long)
                    googleMap?.addMarker(MarkerOptions().position(ll).title(data?.name))
                    googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 2.5f))
                    textCountryName.text = data?.name
                    textCountryCapital.text = data?.capital
                    textCountryCallCode.text = data?.callingCodes?.get(0)
                    textCountryPopulation.text = data?.population?.toString()
                    progressDialog.dismiss()
                }

                override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                    progressDialog.dismiss()
                    Toast.makeText(this@MapsActivity, "Error: " + t.localizedMessage, Toast.LENGTH_LONG).show()
                }
            })
            // search by the given country name, and
            // 1. pin point to the map
            // 2. set the country information to the textViews.
        }
    }

    private fun loadMap() {
        mapFragment!!.getMapAsync { googleMap -> this@MapsActivity.googleMap = googleMap }
    }

    override fun onMapReady(p0: GoogleMap?) {
    }
}
