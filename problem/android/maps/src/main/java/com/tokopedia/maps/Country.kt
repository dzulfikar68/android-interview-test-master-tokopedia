package com.tokopedia.maps

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Country  (
                    val name: String?,
                    val capital: String?,
                    val population: Long?,
                    val callingCodes: List<String>,
                    val latlng: List<Double>
                    ): Parcelable