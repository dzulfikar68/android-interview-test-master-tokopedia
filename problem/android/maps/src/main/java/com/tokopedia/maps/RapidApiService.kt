package com.tokopedia.maps

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path

interface RapidApiService {
    @GET("name/{country}")
    fun getCountryByName(
            @Header("x-rapidapi-host") host: String = "restcountries-v1.p.rapidapi.com",
            @Header("x-rapidapi-key") key: String = "06f2f0a5cbmsh1ea5e65e3796f8cp1bb3adjsn6cf46db330dd",
            @Path("country") country: String
    ): Call<List<Country>>
}